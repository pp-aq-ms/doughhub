FROM node:16-alpine

WORKDIR /usr/src/app

COPY ./app/package*.json ./
RUN npm install
COPY ./app .


# Define the command to run the app
CMD [ "node", "app.js" ]
