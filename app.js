const express = require('express');
const app = express();
const port = process.env.PORT || 3000;

app.get('/', (req, res) => {
    res.send('<h1>Container Bakeries in the Cloud</h1><p>This is a simple app to demonstrate CI/CD with GitLab and Docker.</p>');
});

app.listen(port, () => {
    console.log(`App listening at http://localhost:${port}`);
});
